"""Stream type classes for tap-zalando."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_zalando.client import zalandoStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class OrdersStream(zalandoStream):
    """Define custom stream."""
    name = "orders"
    path = "/merchants/{{merchantId}}/orders"
    primary_keys = ["id"]
    replication_key = "modified_at"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        # Order
        th.Property("type", th.StringType),
        th.Property("id", th.StringType),
        th.Property("merchant_id", th.StringType),
        th.Property("logistics_provider_id", th.StringType),
        th.Property("order_id", th.StringType),
        th.Property("order_number", th.StringType),
        th.Property("order_date", th.DateTimeType),
        th.Property("merchant_order_id", th.StringType),
        th.Property("sales_channel_id", th.StringType),
        th.Property("locale", th.StringType),
        th.Property("customer_number", th.StringType),
        th.Property("status", th.StringType),
        th.Property("shipment_number", th.StringType),
        th.Property("shipping_address", th.ObjectType(
            th.Property("address_type", th.StringType),
            th.Property("address_id", th.StringType),
            th.Property("first_name", th.StringType),
            th.Property("last_name", th.StringType),
            th.Property("address_line_1", th.StringType),
            th.Property("address_line_2", th.StringType),
            th.Property("zip_code", th.StringType),
            th.Property("city", th.StringType),
            th.Property("country_code", th.StringType),
            )
        ),
        th.Property("billing_address", th.ObjectType(
            th.Property("address_type", th.StringType),
            th.Property("address_id", th.StringType),
            th.Property("first_name", th.StringType),
            th.Property("last_name", th.StringType),
            th.Property("address_line_1", th.StringType),
            th.Property("address_line_2", th.StringType),
            th.Property("zip_code", th.StringType),
            th.Property("city", th.StringType),
            th.Property("country_code", th.StringType),
            )
        ),
        th.Property("tracking_number", th.StringType),
        th.Property("return_tracking_number", th.StringType),
        th.Property("order_lines_count", th.IntegerType),
        th.Property("order_lines_price_amount", th.NumberType),
        th.Property("order_lines_price_currency", th.StringType),
        th.Property("delivery_end_date", th.DateTimeType),
        th.Property("exported", th.BooleanType),
        th.Property("created_by", th.StringType),
        th.Property("created_at", th.DateTimeType),
        th.Property("modified_by", th.StringType),
        th.Property("modified_at", th.DateTimeType),
        th.Property("order_type", th.StringType),
        th.Property("customer_email", th.StringType),
        th.Property("stock_location_id", th.StringType),
        th.Property("order_transitions", th.ArrayType(
            th.ObjectType(
                th.Property("type", th.StringType),
                th.Property("id", th.StringType),
                th.Property("order_id", th.StringType),
                th.Property("final_status", th.StringType),
                th.Property("created_at", th.DateTimeType),
                th.Property("created_by", th.StringType),
                )
            )
        ),
        th.Property("order_items", th.ArrayType(
            th.ObjectType(
                th.Property("type", th.StringType),
                th.Property("id", th.StringType),
                th.Property("order_item_id", th.StringType),
                th.Property("order_id", th.StringType),
                th.Property("article_id", th.StringType),
                th.Property("external_id", th.StringType),
                th.Property("description", th.StringType),
                th.Property("quantity_initial", th.IntegerType),
                th.Property("quantity_reserved", th.IntegerType),
                th.Property("quantity_shipped", th.IntegerType),
                th.Property("quantity_returned", th.IntegerType),
                th.Property("quantity_canceled", th.IntegerType),
                th.Property("created_by", th.StringType),
                th.Property("created_at", th.DateTimeType),
                th.Property("modified_by", th.StringType),
                th.Property("modified_at", th.DateTimeType),
                th.Property("order_items_lines", th.ArrayType(
                    th.ObjectType(
                        th.Property("type", th.StringType),
                        th.Property("id", th.StringType),
                        th.Property("order_line_id", th.StringType),
                        th.Property("order_item_id", th.StringType),
                        th.Property("status", th.StringType),
                        th.Property("price", th.ObjectType(
                            th.Property("amount", th.NumberType),
                            th.Property("currency", th.StringType),
                            )
                        ),
                        th.Property("discounted_price", th.ObjectType(
                            th.Property("amount", th.NumberType),
                            th.Property("currency", th.StringType),
                            )
                        ),
                        th.Property("created_by", th.StringType),
                        th.Property("created_at", th.DateTimeType),
                        th.Property("modified_by", th.StringType),
                        th.Property("modified_at", th.DateTimeType),
                        th.Property("source_stock_location_id", th.StringType),
                        th.Property("order_transitions", th.ArrayType(
                            th.ObjectType(
                                th.Property("type", th.StringType),
                                th.Property("id", th.StringType),
                                th.Property("order_line_id", th.StringType),
                                th.Property("final_status", th.StringType),
                                th.Property("created_by", th.StringType),
                                th.Property("created_at", th.DateTimeType),
                                )
                            ),
                        ),
                    ),
                )),
            ),
        )),
    ).to_dict()
