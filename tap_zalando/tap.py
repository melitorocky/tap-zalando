"""zalando tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_zalando.streams import (
    OrdersStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    OrdersStream
]


class Tapzalando(Tap):
    """zalando tap class."""
    name = "tap-zalando"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property("base_path", th.StringType, required=True),
        th.Property("clientId", th.StringType, required=True),
        th.Property("clientSecret", th.StringType, required=True),
        th.Property("merchantId", th.StringType, required=True)
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
