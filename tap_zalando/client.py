"""Custom client handling, including zalandoStream base class."""

import requests,json, psycopg2
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, cast

from .class_orders import Orders

from singer_sdk.streams import Stream

from datetime import datetime
from datetime import timedelta
from dateutil import parser
import pendulum

# import pprint
# pp = pprint.PrettyPrinter(indent=2)

import logging
LOGGER = logging.getLogger(__name__)

class zalandoStream(Stream):
    """Stream class for zalando streams."""

    is_sorted = True

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        streamName = type(self).__name__

        # map the inputs to the function blocks
        if streamName == 'OrdersStream':
            return self.OrdersStream(context)
        else:
            print(streamName + ' not found..')
            quit()


    def get_credentials(self):
        credentials = {
            'base_path': self.config["base_path"],
            'clientId': self.config["clientId"],
            'clientSecret': self.config["clientSecret"],
            'merchantId': self.config["merchantId"],
            'maxNumOrders': self.config["maxNumOrders"]
        }
        return credentials


    def moveKeyUp(self, dictInput, moveKey):
        fixedDict = {}
        for key, val in dictInput.items():
            if key == moveKey:
                for innerKey, innerVal in val.items():
                    fixedDict[innerKey] = innerVal
            else:
                fixedDict[key] = val

        return fixedDict


    def fixListInnerDict(self, listInput, moveKey):
        fixedList = []

        for dictInner in listInput:
            fixedList.append(self.moveKeyUp(dictInner, moveKey))

        return fixedList


    def OrdersStream(self,context):
        last_updated_after = self.getLastReplicationKey(context)
        credentials = self.get_credentials()
        # last_updated_after= '2021-07-15T01:55:47Z'

        OrderClass = Orders(credentials=credentials)

        # getOrders
        res = OrderClass.get_orders(
            merchantId = credentials['merchantId'],
            last_updated_after = last_updated_after,
            maxNumOrders = credentials['maxNumOrders'],
            sort = "modified_at"
        )

        # Move the "attributes" key inner values one level up
        listOrders = self.fixListInnerDict(res["data"], 'attributes')
        ordersNumber = res["meta"]["number_of_results"] # In case we need to do stuff here
        finalRes = []

        for order in listOrders:

            listFixedOrderTransitions = []
            listFixedOrderItems = []
            listFixedOrderItemsLines = []
            listFixedOrderItemsLinesTransitions = []

            transitionsUrl = order["relationships"]["order_transitions"]["links"]["related"]
            itemsUrl = order["relationships"]["order_items"]["links"]["related"]

            # Remove unwanted keys
            order.pop("links", None)
            order.pop("relationships", None)

            # Transitons
            res = OrderClass.get_request(transitionsUrl)
            listOrderTransitions = self.fixListInnerDict(res["data"], 'attributes')

            for transition in listOrderTransitions:
                transition.pop("links", None)
                listFixedOrderTransitions.append(transition)

            order["order_transitions"] = listFixedOrderTransitions


            # Order Items
            res = OrderClass.get_request(itemsUrl)
            listOrderItems = self.fixListInnerDict(res["data"], 'attributes')

            for orderItem in listOrderItems:
                orderLinesUrl = orderItem["relationships"]["order_lines"]["links"]["related"]

                # Remove unwanted keys
                orderItem.pop("links", None)
                orderItem.pop("relationships", None)

                # OrderLines
                res = OrderClass.get_request(orderLinesUrl)
                listOrderLines = self.fixListInnerDict(res["data"], 'attributes')


                for orderLine in listOrderLines:
                    orderLineTransitionsUrl = orderLine["relationships"]["order_line_transitions"]["links"]["related"]

                    # Remove unwanted keys
                    orderLine.pop("links", None)
                    orderLine.pop("relationships", None)
                    orderLine.pop("meta", None)

                    # OrderLineTransitions
                    res = OrderClass.get_request(orderLineTransitionsUrl)
                    listOrderLineTransitions = self.fixListInnerDict(res["data"], 'attributes')

                    for orderLineTransition in listOrderLineTransitions:
                        orderTransition = self.moveKeyUp(orderLineTransition, 'attributes')
                        # Remove unwanted keys
                        orderTransition.pop("links", None)
                        listFixedOrderItemsLinesTransitions.append(orderTransition)

                    orderLine["order_transitions"] = listFixedOrderItemsLinesTransitions
                    listFixedOrderItemsLines.append(orderLine)


                orderItem["order_items_lines"] = listFixedOrderItemsLines
                listFixedOrderItems.append(orderItem)

            order["order_items"] = listFixedOrderItems

            finalRes.append(order)

        return finalRes


    def getLastReplicationKey(self,context): 
        replication_key = self.get_starting_timestamp(context)
        pendulum_start_date = cast(datetime, pendulum.parse(self.config["start_date"]))

        if replication_key == pendulum_start_date:
            jobId = self.config["jobId"]
            bookmark = self.config["bookmark"]
            replication_key = self.getLastSuccessfulRunReplicationKey(jobId=jobId, bookmark=bookmark, start_date=replication_key)

        #trasformazioni su start_date: aggiungo un secondo, trasformo in isoformat
        replication_key = replication_key + timedelta(seconds=1)
        replication_key = replication_key.isoformat()

        return replication_key

    def getLastSuccessfulRunReplicationKey(self, jobId, bookmark, start_date):
        replication_key = start_date
        jsonPayload = '{}'

        conn = None
        try:
            # DB Connection
            conn = psycopg2.connect(
                host=self.config["postgres_remote_host"],
                database=self.config["postgres_remote_database"],
                user=self.config["postgres_remote_user"],
                password=self.config["postgres_remote_password"]
            )

            # Create a cursor
            cur = conn.cursor()

            # Define the query
            query = f"""SELECT payload
                FROM runs
                WHERE
                job_name = '{jobId}' AND
                state = 'SUCCESS'
                ORDER BY id DESC
                LIMIT 1"""

            # Execute the query
            cur.execute(query)

            # Fetch the first row
            queryResult = cur.fetchone()

            if queryResult:
                jsonPayload = queryResult[0]

            # Close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            quit()
        finally:
            if conn is not None:
                conn.close()
                # print('Database connection closed.')

        dictPayload = json.loads(jsonPayload)

        if self.keys_exists(dictPayload, "singer_state", "bookmarks", bookmark, "replication_key_value"):
            replication_key = dictPayload['singer_state']['bookmarks'][bookmark]['replication_key_value']
            replication_key = cast(datetime, pendulum.parse(replication_key))

        return replication_key


    def keys_exists(self, element, *keys):
        '''Check if *keys (nested) exists in `element` (dict).'''
        if not isinstance(element, dict):
            raise AttributeError('keys_exists() expects dict as first argument.')
        if len(keys) == 0:
            raise AttributeError('keys_exists() expects at least two arguments, one given.')

        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True

