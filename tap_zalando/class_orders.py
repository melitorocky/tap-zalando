import requests, json, urllib
from requests.auth import HTTPBasicAuth

class Orders:
    # scheme = 'https://'
    # method = 'GET'
    # content_type = 'application/x-www-form-urlencoded;charset=UTF-8'
    # user_agent = 'python-zalando-api'

    basePath = ''
    clientId = ''
    clientSecret = ''
    merchantId = ''

    accessToken = ''


    def __init__(self, credentials=None):
        self.basePath = credentials['base_path']
        self.clientId = credentials['clientId']
        self.clientSecret = credentials['clientSecret']
        self.merchantId = credentials['merchantId']
        self.accessToken = self.get_access_token()


    def get_access_token(self):
        accessToken = ''

        endpoint = 'https://' + self.basePath + '/auth/token'
        headers = {
            'content-type'  :   'application/x-www-form-urlencoded'
        }
        params = {
            'grant_type'    : 'client_credentials',
            'scope'         : 'access_token_only'
        }

        response = requests.post(
            endpoint,
            auth=HTTPBasicAuth(self.clientId, self.clientSecret),
            headers=headers,
            data=urllib.parse.urlencode(params)
        )

        # Check if positive response
        if response.status_code == 200:
            jsonResponse = response.json()
            accessToken = jsonResponse['access_token']
        else:
            print("OH NO, NON MI POSSO AUTENTICARE!")
            print("Codice: ")
            print(response.status_code)
            quit()

        
        return accessToken
    

    def get_orders(self, **kwargs):
        return self._request(kwargs.pop('merchantId'), params={**kwargs})

    def _request(self, merchantId: str, params: dict = None):

        dictOrders = {}
        if "maxNumOrders" in params:
            params["page[size]"] = params.pop("maxNumOrders")

        endpoint = 'https://' + self.basePath + '/merchants/' + merchantId + '/orders'
        headers = {
            'content-type'  :   'application/x-www-form-urlencoded',
            'Authorization' :   'Bearer ' + self.accessToken
        }


        response = requests.get(
            endpoint,
            headers=headers,
            params=params
        )

        # Check if positive response
        if response.status_code == 200:
            dictOrders = json.loads(response.text)
        else:
            print("OH NO, NON ACCEDO AGLI ORDINI!")
            print("Codice: ")
            print(response.status_code)
            quit()

        return dictOrders

    def get_request(self, url):
        params = {}

        headers = {
            'content-type'  :   'application/x-www-form-urlencoded',
            'Authorization' :   'Bearer ' + self.accessToken
        }

        response = requests.get(
            url,
            headers=headers,
            params=params
        )

        # Check if positive response
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            print("OH NO, NON ACCEDO AGLI ORDINI!")
            print("Codice: " + response.status_code)
            quit()
            
        return 0

